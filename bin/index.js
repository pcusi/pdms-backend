#!/usr/bin/env node
const os = require('os');
const fs = require('fs');
const prompt = require('prompt');
const colors = require('@colors/colors/safe');
const { execSync } = require('child_process');
const dotenv = require('dotenv');
const inquirer = require('inquirer');

dotenv.config();

const BACKEND_SCHEMA = {
  properties: {
    authToken: {
      name: 'token',
      description: colors.blue('What is your gitlab personal access token?'),
      type: 'string',
      required: true,
      hidden: true,
      ask: () => {
        const has_token = validateAuthToken();

        if (has_token) return false;

        return true;
      },
    },
    username: {
      name: 'username',
      description: colors.white('What is your gitlab user?'),
      type: 'string',
      required: true,
      ask: () => {
        const has_token = validateAuthToken();

        if (has_token) return false;

        return true;
      },
    },
    folder: {
      name: 'folder',
      description: colors.white(
        'What name do you want to put at your microservice?',
      ),
      type: 'string',
      required: false,
    },
  },
};

const main = async () => {
  try {
    prompt.start();

    prompt.message = colors.rainbow('');
    prompt.delimiter = colors.green(': ');

    const { type } = await inquirer.prompt(
      {
        type: 'list',
        name: 'type',
        message: 'What type of project is?',
        choices: ['Frontend', 'Backend'],
      },
      {},
    );

    await prompt.get(BACKEND_SCHEMA, (_, values) => {
      if (values === undefined) return;

      if (values.authToken !== undefined && values.username) {
        storeAuthToken(values.authToken, values.username);
      }

      if (values.folder !== undefined) {
        createDirectory({
          folder: values.folder,
          user: values.username,
          token: values.authToken,
          type,
        });
      }
    });
  } catch (e) {
    console.log(e);
  }
};

const createDirectory = ({ folder, user, token, type }) => {
  const path = `${os.userInfo().homedir}/Documents/`;

  if (!fs.existsSync(`${path}/${folder}`)) {
    fs.mkdirSync(`${path}/${folder}`);

    cloneRepository({
      folder,
      user,
      token,
      type,
    });
  } else {
    console.log(
      colors.red(
        "Folder exists please ensure that the folder wasn't created before.",
      ),
    );
  }
};

const storeAuthToken = (token, username) => {
  fs.writeFileSync(
    `${process.cwd()}/.env`,
    `PDMS_GITLAB_TOKEN=${token}
PDMS_GITLAB_USER=${username}
  `,
  );
};

const validateAuthToken = () => {
  return fs.existsSync(`${process.cwd()}/.env`);
};

const cloneRepository = ({ folder, user, token, type }) => {
  console.log(colors.green(`--- Cloning boilerplate ${type} in ${folder} ---`));
  console.log(type);

  const repo =
    type === 'Backend' ? 'pd-api-boilerpate' : 'pd-backoffice-boilerplate-react';

  execSync(
    `cd ${os.userInfo().homedir}/Documents/${folder} && git clone https://${
      user !== '' ? user : process.env.PDMS_GITLAB_USER
    }:${token !== '' ? token : process.env.PDMS_GITLAB_TOKEN}@gitlab.com/${
      user !== '' ? user : process.env.PDMS_GITLAB_USER
    }/${repo}.git`,
  );

  console.log(colors.green(`--- Moving files to ${folder} ---`));

  execSync(
    `mv -v ${os.userInfo().homedir}/Documents/${folder}/${repo}/* ${
      os.userInfo().homedir
    }/Documents/${folder} && rm -rf ${
      os.userInfo().homedir
    }/Documents/${folder}/${repo}`,
  );

  console.log(colors.green(`--- Installing dependencies ${folder} ---`));

  execSync(`cd ${os.userInfo().homedir}/Documents/${folder} && npm i`);

  console.log(colors.white(`--- Dependencies installed ---`));
};

main().then((r) => r);
